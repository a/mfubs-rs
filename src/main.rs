// MFUbs-RS: Tool written in Rust to talk to Mifare Ultralight and compatible cards with an ACR122U
// Copyright (C) 2020 Ave Ozkal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate pcsc;
extern crate hex;
extern crate crc_all;

use std::env;
use pcsc::*;
use hex::FromHex;
use crc_all::Crc;

fn send_apdu(apdu: &[u8], card: &pcsc::Card) -> std::vec::Vec<u8> {
    let mut rapdu_buf = [0; MAX_BUFFER_SIZE];
    let rapdu = match card.transmit(apdu, &mut rapdu_buf) {
        Ok(rapdu) => rapdu,
        Err(err) => {
            eprintln!("Failed to transmit APDU command to card: {}", err);
            std::process::exit(1);
        }
    };
    // Hacky!
    rapdu.to_vec()
}

fn do_crc_a(data: &[u8]) -> std::vec::Vec<u8> {
    // Pain!
    let mut digest = Crc::<u16>::new(0x1021, 16, 0xC6C6, 0x0, true);
    digest.update(data).to_le_bytes().to_vec()
}

fn connect() -> pcsc::Card {
    // Establish a PC/SC context.
    let ctx = match Context::establish(Scope::User) {
        Ok(ctx) => ctx,
        Err(err) => {
            eprintln!("Failed to establish context: {}", err);
            std::process::exit(1);
        }
    };

    // List available readers.
    let mut readers_buf = [0; 2048];
    let mut readers = match ctx.list_readers(&mut readers_buf) {
        Ok(readers) => readers,
        Err(err) => {
            eprintln!("Failed to list readers: {}", err);
            std::process::exit(1);
        }
    };

    // Use the first reader.
    // TODO: Can we ensure that we pick an ACR122U, or at least not a Yubikey?
    let reader = match readers.next() {
        Some(reader) => reader,
        None => {
            println!("No readers are connected.");
            std::process::exit(1);
        }
    };
    println!("Using reader: {:?}", reader);

    // Connect to the card.
    let card = match ctx.connect(reader, ShareMode::Shared, Protocols::ANY) {
        Ok(card) => card,
        Err(Error::NoSmartcard) => {
            println!("A smartcard is not present in the reader.");
            std::process::exit(1);
        }
        Err(err) => {
            eprintln!("Failed to connect to card: {}", err);
            std::process::exit(1);
        }
    };
    
    card
}

fn read_data(card: &pcsc::Card, block: u8, do_crc: bool) -> std::vec::Vec<u8> {
    let write_cmd = b"\x30";
    let mut full_cmd = write_cmd.to_vec();
    full_cmd.extend(&[block]);

    if do_crc {
        let crc_bytes = do_crc_a(&full_cmd);
        full_cmd.extend(crc_bytes);
    }

    let mut out = send_apdu(&full_cmd, &card);
    out.truncate(4);
    out
}

fn write_data(card: &pcsc::Card, block: u8, data: &[u8], do_crc: bool) {
    let write_cmd = b"\xA2";
    let mut full_cmd = write_cmd.to_vec();
    full_cmd.extend(&[block]);
    full_cmd.extend(data);

    if do_crc {
        let crc_bytes = do_crc_a(&full_cmd);
        full_cmd.extend(crc_bytes);
    }

    send_apdu(&full_cmd, &card);
}

fn write_data_comp(card: &pcsc::Card, block: u8, data: &[u8], do_crc: bool) {
    let write_cmd = b"\xA0";
    let otp_padding = b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".to_vec();
    let mut full_cmd = write_cmd.to_vec();
    full_cmd.extend(&[block]);
    full_cmd.extend(data);
    full_cmd.extend(&otp_padding);

    if do_crc {
        let crc_bytes = do_crc_a(&full_cmd);
        full_cmd.extend(crc_bytes);
    }

    send_apdu(&full_cmd, &card);
}

fn get_otp(card: &pcsc::Card) -> std::vec::Vec<u8>  {
    read_data(&card, 3, true)
}

fn print_block(card: &pcsc::Card, block: u8, do_crc: bool) {
    let out = read_data(&card, block, do_crc);
    println!("[{}] {}", hex::encode_upper(&[block]), hex::encode_upper(&out));
}

fn increase_otp(card: &pcsc::Card, which_otp: usize, comp: bool, do_crc: bool) {
    let mut otp = get_otp(&card);

    otp[which_otp] += 1;

    // Write to block 3 (OTP block)
    if comp {
        write_data_comp(&card, 3, &otp, do_crc);
    }
    else
    {
        write_data(&card, 3, &otp, do_crc);
    }

    println!("Done.");
    std::thread::sleep(std::time::Duration::from_secs(1));
    print_block(&card, 3, true);
}

fn main() {
    println!("MFUbs-rs, developed by aveao, released under GPLv3.\nhttps://gitlab.com/a/mfubs-rs\n");

    let card = connect();

    let args: Vec<String> = env::args().collect();

    match args.len() {
        4 => {
            let arg1: String = args[1].parse().expect("Invalid input (??? how)");
            let arg2: String = args[2].parse().expect("Invalid input (??? how)");
            let arg3: String = args[3].parse().expect("Invalid input (??? how)");

            let arg2hex: u8 = Vec::from_hex(arg2).expect("Input isn't a valid hex")[0];
            let arg3hex: Vec<u8> = Vec::from_hex(arg3).expect("Input isn't a valid hex");

            if arg1 == "write" {
                write_data(&card, arg2hex, &arg3hex, true);
            }
            else if arg1 == "writecomp" {
                write_data_comp(&card, arg2hex, &arg3hex, true);
            }
            else if arg1 == "writenocrc" {
                write_data(&card, arg2hex, &arg3hex, false);
            }
            else if arg1 == "writecompnocrc" {
                write_data_comp(&card, arg2hex, &arg3hex, false);
            }
        },
        3 => {
            let arg1: String = args[1].parse().expect("Invalid input (??? how)");
            let arg2: String = args[2].parse().expect("Invalid input (??? how)");

            let arg2hex: u8 = Vec::from_hex(arg2).expect("Input isn't a valid hex")[0];

            if arg1 == "incotp" {
                increase_otp(&card, arg2hex.into(), false, true);
            }
            else if arg1 == "incotpcomp" {
                increase_otp(&card, arg2hex.into(), true, true);
            }
            else if arg1 == "incotpnocrc" {
                increase_otp(&card, arg2hex.into(), false, false);
            }
            else if arg1 == "incotpcompnocrc" {
                increase_otp(&card, arg2hex.into(), true, false);
            }
            else if arg1 == "readblock" {
                print_block(&card, arg2hex, true);
            }
            else if arg1 == "readblocknocrc" {
                print_block(&card, arg2hex, false);
            }
        },
        2 => {
            let arg: String = args[1].parse().expect("Invalid input (??? how)");

            if arg == "help" {
                println!("help - display this text");
                println!("read - read the whole card");
                println!("readblock <block> - read the given block");
                println!("write <block> <bytes as hex> - write the data to the given block");
                println!("incotp [byte] - increase OTP on block 3, on given byte (default: 0)");
                println!("== Compatible Mode Commands ==");
                println!("writecomp <block> <bytes as hex> - write the data to the given block");
                println!("incotpcomp [byte] - increase OTP on block 3, on given byte (default: 0)");
                println!("== No CRC commands ==");
                println!("readnocrc - read the whole card");
                println!("readblocknocrc <block> - read the given block");
                println!("writenocrc <block> <bytes as hex> - write the data to the given block");
                println!("incotpnocrc [byte] - increase OTP on block 3, on given byte (default: 0)");
                println!("== Compatible Mode No CRC commands ==");
                println!("writecompnocrc <block> <bytes as hex> - write the data to the given block");
                println!("incotpcompnocrc [byte] - increase OTP on block 3, on given byte (default: 0)");
            }
            else if arg == "incotp" {
                increase_otp(&card, 0, false, true);
            }
            else if arg == "incotpcomp" {
                increase_otp(&card, 0, true, true);
            }
            else if arg == "incotpnocrc" {
                increase_otp(&card, 0, false, false);
            }
            else if arg == "incotpcompnocrc" {
                increase_otp(&card, 0, true, false);
            }
            else if arg == "read" {
                // Read for 16 blocks
                for block in 0..16 {
                    print_block(&card, block, true);
                }
            }
            else if arg == "readnocrc" {
                // Read for 16 blocks
                for block in 0..16 {
                    print_block(&card, block, false);
                }
            }
        },
        _ => {
            println!("No args provided (try: help).")
        }
    }
}
