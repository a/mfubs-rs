# MFUbs-rs

Tool to mess with Mifare Ultralight (and compatible) cards with an ACR122U.

Code is based on [flashuid-rs](https://gitlab.com/a/flashuid-rs).

Keep in mind that I'm not great with Rust, so expect some issues here and there with the code. Only tested on Linux.

Auth stuff is currently unimplemented. Cards with more or less than 16 blocks are unimplemented. CRC bytes on responses are not verified. Writes are not confirmed. Compatible read is not implemented yet.

## Usage

Plug in an ACR122U and run mfubs-rs:

- `help` - display help text
- `read` - read the whole card
- `readblock <block>` - read the given block
- `write <block> <bytes as hex>` - write the data to the given block
- `incotp [byte]` - increase OTP on block 3, on given byte (default: 0)
- == Compatible Mode Commands ==
- `writecomp <block> <bytes as hex>` - write the data to the given block, but use compatibility mode write command
- `incotpcomp [byte]` - increase OTP on block 3, on given byte (default: 0), but use compatibility mode write command
- == No CRC commands ==
- `readnocrc` - read the whole card
- `readblocknocrc <block>` - read the given block
- `writenocrc <block> <bytes as hex>` - write the data to the given block
- `incotpnocrc [byte]` - increase OTP on block 3, on given byte (default: 0)
- == Compatible Mode No CRC Commands ==
- `writecompnocrc <block> <bytes as hex>` - write the data to the given block, but use compatibility mode write command
- `incotpcompnocrc [byte]` - increase OTP on block 3, on given byte (default: 0), but use compatibility mode write command
